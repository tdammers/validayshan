define (['jquery'], function($){
    var Validayshan = function(namespace) {
        validayshan = this;
        this.namespace = namespace;
        if (this.namespace)
            this.prefix = this.namespace + '-';
        else
            this.prefix = '';

        var regexValidate = function(value, re) { return (value.trim() === '') || re.test(value); };

        this.validators = {
            'required': function(value, elem) {
                return value.trim() !== '';
            },
            'regex': function(value, elem) {
                return regexValidate(value, new RegExp(elem.data(validayshan.cl('regex'))));
            },
            'int': function(value, elem) {
                return regexValidate(value, /^\s*-?(0|[1-9][0-9]*)\s*$/);
            },
            'decimal': function(value, elem) {
                return regexValidate(value, /^\s*-?(0|[1-9][0-9]*)(\.[0-9]*)?\s*$/);
            },
            'alphanumeric': function(value, elem) {
                return regexValidate(value, /^[a-zA-Z0-9]*$/);
            },
        };
    };

    Validayshan.prototype.cl = function(name) {
        if (name)
            return this.prefix + name;
        else
            return this.namespace;
    }

    Validayshan.prototype.cls = function(name) { return '.' + this.cl(name); }

    Validayshan.prototype.validateField = function(elem, force) {
        if (!elem.hasClass(this.cl('invalid')) && !force) {
            return true;
        }
        var value = $(this.cls('target'), elem).val();
        var valid = true;
        var validators = $(this.cls('validator'), elem);
        var v = this;
        validators.each(function(){
            var type = $(this).data(v.cl('type'));
            var f = v.validators[type];
            if (f) {
                if (f(value, $(this))) {
                    $(this).removeClass(v.cl('active'));
                }
                else {
                    valid = false;
                    $(this).addClass(v.cl('active'));
                }
            }
            else {
                console.log("No validator found for " + type);
            }
        });
        if (valid)
            elem.removeClass(this.cl('invalid'));
        else
            elem.addClass(this.cl('invalid'));
        return valid;
    };

    Validayshan.prototype.validateForm = function(frm) {
        var valid = true;
        var elems = $(this.cls('field'), frm);
        var v = this;
        elems.each(function() { if (!v.validateField($(this), true)) valid = false; });
        return valid;
    };

    return {
        'attach': function(namespace) {
            $(document).ready(function(){
                var validayshan = new Validayshan(namespace);

                $('body').on('submit', 'form', function(e) {
                    var isValid = validayshan.validateForm($(this));
                    if (!isValid) {
                        e.preventDefault();
                    }
                });
                $('body').on('keyup', validayshan.cls('field'), function(e) {
                    validayshan.validateField($(this));
                });
                $('body').on('change', validayshan.cls('field'), function(e) {
                    validayshan.validateField($(this), true);
                });
            });
        }
    };
});
